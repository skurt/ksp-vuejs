new Vue({
	// http://ksp.skurt.de/auswahl.php
  // https://knsu.uni-koblenz.de/sport_und/bewegung/th_koerperschwerpunkt_-_bedeutung_und_bestimmung/th_koerperschwerpunkt_-_bedeutung_und_bestimmung.pdf
  el: "#app",
  data: {
    image:'',
    canvas: null,
    context: null,
    isDrawing: false,
    currentJointPointIndex: 0,
    nextJointPoint: { name:"Fussspitzen rechts", x:0, y:0 },
    jointPoints: { //https://vuejs.org/v2/guide/#Getting-Started
      fusp_re: { name: "Fussspitzen rechts", x:0, y:0, index:0 },
      knoe_re: { name: "Knöchel rechts", x:0, y:0, index:2 },
      knie_re: { name: "Knie rechts", x:0, y:0, index:4 },
      huef_re: { name: "Hüfte rechts", x:0, y:0, index:6 },
      huef_li: { name: "Hüfte links", x:0, y:0, index:7 },
      knie_li: { name: "Knie links", x:0, y:0, index:5 },
      knoe_li: { name: "Knöchel links", x:0, y:0, index:3 },
      fusp_li: { name: "Fussspitzen links", x:0, y:0, index:1 },
      fing_re: { name: "Fingerspitzen rechts", x:0, y:0, index:14 },
      hand_re: { name: "Handwurzel rechts", x:0, y:0, index:12 },
      ellb_re: { name: "Ellenbogen rechts", x:0, y:0, index:10 },
      schu_re: { name: "Schulter rechts", x:0, y:0, index:8 },
      schu_li: { name: "Schultern links", x:0, y:0, index:9 },
      ellb_li: { name: "Ellenbogen links", x:0, y:0, index:11 },
      hand_li: { name: "Handwurzel links", x:0, y:0, index:13 },
      fing_li: { name: "Fingerspitzen links", x:0, y:0, index:15 },
      kopf: { name: "Kopfmitte", x:0, y:0, index:16 },
      end: { name: "...", index:17}
    },
    lines: {
    	fuss_re: { name: "Fuss rechts", center_x:0, center_y:0, weight:1.5, relation:0.44},
      fuss_li: { name: "Fuss links", center_x:0, center_y:0, weight:1.5, relation:0.44},
      uschenk_re: { name: "Unterschenkel rechts", center_x:0, center_y:0, weight:4.5, relation:0.42},
      uschenk_li: { name: "Unterschenkel links", center_x:0, center_y:0, weight:4.5, relation:0.42},
      oschenk_re: { name: "Oberschenkel rechts", center_x:0, center_y:0, weight:14, relation:0.44},
      oschenk_li: { name: "Oberschenkel links", center_x:0, center_y:0, weight:14, relation:0.44},
      hand_re: { name: "Hand rechts", center_x:0, center_y:0, weight:0.7, relation:0.5},
      hand_li: { name: "Hand links", center_x:0, center_y:0, weight:0.7, relation:0.5},
      uarm_re: { name: "Unterarm rechts", center_x:0, center_y:0, weight:1.6, relation:0.42},
      uarm_li: { name: "Unterarm links", center_x:0, center_y:0, weight:1.6, relation:0.42},
      oarm_re: { name: "Oberarm rechts", center_x:0, center_y:0, weight:2.7, relation:0.47},
      oarm_li: { name: "Oberarm links", center_x:0, center_y:0, weight:2.7, relation:0.47},
      rumpf: { name: "Rumpf", center_x:0, center_y:0, weight:43, relation:0.44},
      kopf: { name: "Kopf", center_x:0, center_y:0, weight:7, relation:0.5},
    },
    ksp: { x:0, y:0 }
  },
  mounted(){
    var vm = this
    vm.canvas = vm.$refs.canvas
    vm.context = vm.canvas.getContext("2d")
    vm.canvas.addEventListener('mousedown', vm.mousedown)
  },
  methods: {
  	calculate_ksp: function() {
      this.lines.fuss_re.center_x = Math.round(((1-this.lines.fuss_re.relation)*this.jointPoints.fusp_re.x) + (this.lines.fuss_re.relation*this.jointPoints.knoe_re.x))
      this.lines.fuss_li.center_x = Math.round(((1-this.lines.fuss_li.relation)*this.jointPoints.fusp_li.x) + (this.lines.fuss_li.relation*this.jointPoints.knoe_li.x))
      this.lines.uschenk_re.center_x = Math.round(((1-this.lines.uschenk_re.relation)*this.jointPoints.knoe_re.x) + (this.lines.uschenk_re.relation*this.jointPoints.knie_re.x))
      this.lines.uschenk_li.center_x = Math.round(((1-this.lines.uschenk_li.relation)*this.jointPoints.knoe_li.x) + (this.lines.uschenk_li.relation*this.jointPoints.knie_li.x))
      this.lines.oschenk_re.center_x = Math.round(((1-this.lines.oschenk_re.relation)*this.jointPoints.knie_re.x) + (this.lines.oschenk_re.relation*this.jointPoints.huef_re.x))
      this.lines.oschenk_li.center_x = Math.round(((1-this.lines.oschenk_li.relation)*this.jointPoints.knie_li.x) + (this.lines.oschenk_li.relation*this.jointPoints.huef_li.x))
      this.lines.hand_re.center_x = Math.round(((1-this.lines.hand_re.relation)*this.jointPoints.fing_re.x) + (this.lines.hand_re.relation*this.jointPoints.hand_re.x))
      this.lines.hand_li.center_x = Math.round(((1-this.lines.hand_li.relation)*this.jointPoints.fing_li.x) + (this.lines.hand_li.relation*this.jointPoints.hand_li.x))
      this.lines.uarm_re.center_x = Math.round(((1-this.lines.uarm_re.relation)*this.jointPoints.ellb_re.x) + (this.lines.uarm_re.relation*this.jointPoints.hand_re.x))
      this.lines.uarm_li.center_x = Math.round(((1-this.lines.uarm_li.relation)*this.jointPoints.ellb_li.x) + (this.lines.uarm_li.relation*this.jointPoints.hand_li.x))
      this.lines.oarm_re.center_x = Math.round(((1-this.lines.oarm_re.relation)*this.jointPoints.schu_re.x) + (this.lines.oarm_re.relation*this.jointPoints.ellb_re.x))
      this.lines.oarm_li.center_x = Math.round(((1-this.lines.oarm_li.relation)*this.jointPoints.schu_li.x) + (this.lines.oarm_li.relation*this.jointPoints.ellb_li.x))
      const hueft_x = Math.round(((1-0.5)*this.jointPoints.huef_re.x) + (0.5*this.jointPoints.huef_li.x))
      const schu_x = Math.round(((1-0.5)*this.jointPoints.schu_re.x) + (0.5*this.jointPoints.schu_li.x))
      this.lines.rumpf.center_x = Math.round(((1-this.lines.rumpf.relation)*hueft_x) + (this.lines.rumpf.relation*schu_x))

      this.lines.fuss_re.center_y = Math.round(((1-this.lines.fuss_re.relation)*this.jointPoints.fusp_re.y) + (this.lines.fuss_re.relation*this.jointPoints.knoe_re.y))
      this.lines.fuss_li.center_y = Math.round(((1-this.lines.fuss_li.relation)*this.jointPoints.fusp_li.y) + (this.lines.fuss_li.relation*this.jointPoints.knoe_li.y))
      this.lines.uschenk_re.center_y = Math.round(((1-this.lines.uschenk_re.relation)*this.jointPoints.knoe_re.y) + (this.lines.uschenk_re.relation*this.jointPoints.knie_re.y))
      this.lines.uschenk_li.center_y = Math.round(((1-this.lines.uschenk_li.relation)*this.jointPoints.knoe_li.y) + (this.lines.uschenk_li.relation*this.jointPoints.knie_li.y))
      this.lines.oschenk_re.center_y = Math.round(((1-this.lines.oschenk_re.relation)*this.jointPoints.knie_re.y) + (this.lines.oschenk_re.relation*this.jointPoints.huef_re.y))
      this.lines.oschenk_li.center_y = Math.round(((1-this.lines.oschenk_li.relation)*this.jointPoints.knie_li.y) + (this.lines.oschenk_li.relation*this.jointPoints.huef_li.y))
      this.lines.hand_re.center_y = Math.round(((1-this.lines.hand_re.relation)*this.jointPoints.fing_re.y) + (this.lines.hand_re.relation*this.jointPoints.hand_re.y))
      this.lines.hand_li.center_y = Math.round(((1-this.lines.hand_li.relation)*this.jointPoints.fing_li.y) + (this.lines.hand_li.relation*this.jointPoints.hand_li.y))
      this.lines.uarm_re.center_y = Math.round(((1-this.lines.uarm_re.relation)*this.jointPoints.ellb_re.y) + (this.lines.uarm_re.relation*this.jointPoints.hand_re.y))
      this.lines.uarm_li.center_y = Math.round(((1-this.lines.uarm_li.relation)*this.jointPoints.ellb_li.y) + (this.lines.uarm_li.relation*this.jointPoints.hand_li.y))
      this.lines.oarm_re.center_y = Math.round(((1-this.lines.oarm_re.relation)*this.jointPoints.schu_re.y) + (this.lines.oarm_re.relation*this.jointPoints.ellb_re.y))
      this.lines.oarm_li.center_y = Math.round(((1-this.lines.oarm_li.relation)*this.jointPoints.schu_li.y) + (this.lines.oarm_li.relation*this.jointPoints.ellb_li.y))
      const hueft_y = Math.round(((1-0.5)*this.jointPoints.huef_re.y) + (0.5*this.jointPoints.huef_li.y))
      const schu_y = Math.round(((1-0.5)*this.jointPoints.schu_re.y) + (0.5*this.jointPoints.schu_li.y))
      this.lines.rumpf.center_y = Math.round(((1-this.lines.rumpf.relation)*hueft_y) + (this.lines.rumpf.relation*schu_y))
      
     	this.drawCoordinates(this.lines.fuss_re.center_x,this.lines.fuss_re.center_y)
     	this.drawCoordinates(this.lines.fuss_li.center_x,this.lines.fuss_li.center_y)
     	this.drawCoordinates(this.lines.uschenk_re.center_x,this.lines.uschenk_re.center_y)
     	this.drawCoordinates(this.lines.uschenk_li.center_x,this.lines.uschenk_li.center_y)
     	this.drawCoordinates(this.lines.oschenk_re.center_x,this.lines.oschenk_re.center_y)
     	this.drawCoordinates(this.lines.oschenk_li.center_x,this.lines.oschenk_li.center_y)
     	this.drawCoordinates(this.lines.rumpf.center_x,this.lines.rumpf.center_y)
     	this.drawCoordinates(this.lines.oarm_re.center_x,this.lines.oarm_re.center_y)
     	this.drawCoordinates(this.lines.oarm_li.center_x,this.lines.oarm_li.center_y)
     	this.drawCoordinates(this.lines.uarm_re.center_x,this.lines.uarm_re.center_y)
     	this.drawCoordinates(this.lines.uarm_li.center_x,this.lines.uarm_li.center_y)
     	this.drawCoordinates(this.lines.hand_re.center_x,this.lines.hand_re.center_y)
     	this.drawCoordinates(this.lines.hand_li.center_x,this.lines.hand_li.center_y)
      
      this.lines.kopf.center_x = this.jointPoints.kopf.x
      this.lines.kopf.center_y = this.jointPoints.kopf.y

			this.ksp.x = Math.round((
      this.lines.kopf.center_x*this.lines.kopf.weight+
      this.lines.rumpf.center_x*this.lines.rumpf.weight+
      this.lines.oarm_re.center_x*this.lines.oarm_re.weight+
      this.lines.uarm_re.center_x*this.lines.uarm_re.weight+
      this.lines.hand_re.center_x*this.lines.hand_re.weight+
      this.lines.oarm_li.center_x*this.lines.oarm_li.weight+
      this.lines.uarm_li.center_x*this.lines.uarm_li.weight+
      this.lines.hand_li.center_x*this.lines.hand_li.weight+
      this.lines.oschenk_re.center_x*this.lines.oschenk_re.weight+
      this.lines.uschenk_re.center_x*this.lines.uschenk_re.weight+
      this.lines.fuss_re.center_x*this.lines.fuss_re.weight+
      this.lines.oschenk_li.center_x*this.lines.oschenk_li.weight+
      this.lines.uschenk_li.center_x*this.lines.uschenk_li.weight+
      this.lines.fuss_li.center_x*this.lines.fuss_li.weight)/100)
      console.log(this.ksp.x)

			this.ksp.y = Math.round((
      this.lines.kopf.center_y*this.lines.kopf.weight+
      this.lines.rumpf.center_y*this.lines.rumpf.weight+
      this.lines.oarm_re.center_y*this.lines.oarm_re.weight+
      this.lines.uarm_re.center_y*this.lines.uarm_re.weight+
      this.lines.hand_re.center_y*this.lines.hand_re.weight+
      this.lines.oarm_li.center_y*this.lines.oarm_li.weight+
      this.lines.uarm_li.center_y*this.lines.uarm_li.weight+
      this.lines.hand_li.center_y*this.lines.hand_li.weight+
      this.lines.oschenk_re.center_y*this.lines.oschenk_re.weight+
      this.lines.uschenk_re.center_y*this.lines.uschenk_re.weight+
      this.lines.fuss_re.center_y*this.lines.fuss_re.weight+
      this.lines.oschenk_li.center_y*this.lines.oschenk_li.weight+
      this.lines.uschenk_li.center_y*this.lines.uschenk_li.weight+
      this.lines.fuss_li.center_y*this.lines.fuss_li.weight)/100)
      console.log(this.ksp.y)
      
    	this.drawCoordinates(this.ksp.x, this.ksp.y, "#FFFF00")
    },
    onFileChange(e) {
      var files = e.target.files || e.dataTransfer.files;
      if (!files.length)
        return;
      this.createImage(files[0]);
    },
    createImage(file) {
      var image = new Image();
      var reader = new FileReader();
      var vm = this;

      reader.onload = (e) => {
        //vm.image = e.target.result;
        this.drawImageIntoCanvas(e.target.result)
      };
      reader.readAsDataURL(file);
      
    },
    removeImage: function (e) {
      this.image = '';
    },
    drawImageIntoCanvas(image_url) {
    	//var canvas = document.getElementById("canvas"),
      //ctx = canvas.getContext("2d");

      //canvas.width = 903;
      //canvas.height = 657;

      var background = new Image();
      background.src = image_url;
      
      var ctx = this.canvas.getContext("2d");

      // Make sure the image is loaded first otherwise nothing will draw.
      background.onload = function(){
          ctx.drawImage(background,0,0);   
      }
    },
    mousedown(e){
      //console.log("mousedown")
      var vm = this
      var rect = vm.canvas.getBoundingClientRect();
      var x = e.clientX - rect.left;
      var y = e.clientY - rect.top;

      // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
      const jointPointsKeys = Object.keys(vm.jointPoints)
      const currentJointPoint = vm.jointPoints[jointPointsKeys[vm.currentJointPointIndex++]]
      vm.nextJointPoint = vm.jointPoints[jointPointsKeys[vm.currentJointPointIndex]]
      currentJointPoint.x = x;
      currentJointPoint.y = y;
      this.drawCoordinates(x,y, "#000000");
    },
    resetCanvas(){
      var vm = this
      vm.canvas.width = vm.canvas.width;
      vm.points.length = 0; // reset points array
    },
    saveImage(){
      var vm = this
      var dataURL = vm.canvas.toDataURL();
      var img = vm.$refs.img;
      img.src = dataURL;
    },
    drawCoordinates(x,y, color="#ff2626"){
        var pointSize = 3; // Change according to the size of the point.
        var ctx = this.canvas.getContext("2d");

        ctx.fillStyle = color; // Red color

        ctx.beginPath(); //Start path
        ctx.arc(x, y, pointSize, 0, Math.PI * 2, true); // Draw a point using the arc function of the canvas with a point structure.
        ctx.fill(); // Close the path and fill.
    },
}
})